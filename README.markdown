# Entity Property Tool

This codebase is written to be an easy to use tool to view and manipulate entity properties. To run it locally:

    npm install
    grunt

Then visit: http://localhost:3000/jira/atlassian-connect.json

You can install this addon into your development JIRA hosted application that has Atlassian Connect enabled.

## Acknowledgements

Icons made by [Freepik][1] from [www.flaticon.com][2] is licensed by [CC BY 3.0][3].

 [1]: http://www.freepik.com
 [2]: http://www.flaticon.com
 [3]: http://creativecommons.org/licenses/by/3.0/
